FROM python:3.9.6-alpine3.14

ARG USER_ID=1000
ARG GROUP_ID=1000

ENV UID=${USER_ID}
ENV GID=${GROUP_ID}
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update && apk upgrade

RUN adduser -D pi

RUN apk add shadow && \
    groupmod -g $GROUP_ID pi && \
    usermod -g pi -u $USER_ID pi && \
    apk del shadow

RUN apk add postgresql-dev gcc python3-dev musl-dev

WORKDIR /home/pi/PythonIntegrator
COPY requirements .

RUN pip install --no-cache-dir -r requirements

USER pi
