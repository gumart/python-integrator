import openpyxl

from datetime import datetime

from django.core.exceptions import MultipleObjectsReturned

from integrator.models import Hospital, Doctor, Ticket

class CacheService:

    def __init__(self, filepath):
        self.filepath = filepath

    def cache(self):
        try:
            file = openpyxl.open(self.filepath, read_only=True)

            sheet = file.active

            for row in range(2, sheet.max_row+1):
                if sheet[row][0].value is None:
                    break
                hospital, created = Hospital.objects.get_or_create(
                    id=sheet[row][0].value,
                    defaults = {
                        'address': sheet[row][1].value
                    }
                )

                doctor, created = Doctor.objects.update_or_create(
                    id=sheet[row][2].value,
                    defaults = {
                        'fullname': sheet[row][3].value,
                        'hospital_id': hospital
                    }
                )

                try:
                    ticket, created = Ticket.objects.update_or_create(
                        date=sheet[row][4].value,
                        hospital_id=hospital,
                        doctor_id=doctor,
                        begin_time=sheet[row][5].value,
                        end_time=sheet[row][6].value,
                        defaults={
                            'begin_time': sheet[row][5].value,
                            'end_time': sheet[row][6].value,
                            'is_closed': True if sheet[row][7].value == 'свободен' else False
                        }
                    )
                except MultipleObjectsReturned:
                    Ticket.objects.filter(
                        date=sheet[row][4].value,
                        hospital_id=hospital,
                        doctor_id=doctor
                    ).delete()

                    Ticket.objects.create(
                        date=sheet[row][4].value,
                        hospital_id=hospital,
                        doctor_id=doctor,
                        begin_time=sheet[row][5].value,
                        end_time=sheet[row][6].value,
                        is_closed=True if sheet[row][7].value == 'свободен' else False
                    )
        except ValueError:
            return 'Incorrect data'
        except FileNotFoundError:
            return 'Incorrect file path'
        return 'Success'
