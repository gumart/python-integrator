from django.db import models

# Create your models here.

class Hospital(models.Model):
    id = models.IntegerField('ID поликлиники', primary_key=True)
    address = models.TextField('Адресс')

    def __str__(self):
        return self.address
class Doctor(models.Model):
    id = models.IntegerField('ID врача', primary_key=True)
    fullname = models.CharField('ФИО врача', max_length=100)
    hospital_id = models.ForeignKey(Hospital, verbose_name='Поликлиника', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.fullname

class Ticket(models.Model):
    date = models.DateField('Дата приёма')
    begin_time = models.TimeField('Время начала приёма', null=True)
    end_time = models.TimeField('Время окончания приёма', null=True)
    is_closed = models.BooleanField('Доступность талона', default=True)
    hospital_id = models.ForeignKey(Hospital, verbose_name='Поликлиника', on_delete=models.CASCADE, null=False)
    doctor_id = models.ForeignKey(Doctor, verbose_name='Врач', on_delete=models.SET_NULL, null=True)
