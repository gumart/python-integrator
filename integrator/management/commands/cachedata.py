from django.core.management.base import BaseCommand
from PythonIntegrator import settings

from integrator.Services.CacheService import CacheService

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        cacheService = CacheService(settings.FILE_ROOT)

        result = cacheService.cache()

        self.stdout.write(result)