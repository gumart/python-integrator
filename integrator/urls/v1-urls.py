from django.urls import path

from integrator import views

app_name = 'v1-integrator'
urlpatterns = [
    path('organizations', views.HospitalApiView.as_view(), name='organizations'),
    path('doctors', views.DoctorApiView.as_view(), name='doctors'),
    path('tickets', views.TicketApiView.as_view(), name='tickets')
]