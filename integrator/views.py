from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
import datetime

from integrator.Forms.TicketForm import TicketForm
from integrator.models import Hospital, Doctor, Ticket
from integrator.serializer import HospitalSerializer, DoctorSerializer, TicketsSerializer


class HospitalApiView(APIView):

    def get(self, request):
        hospitals = Hospital.objects.all()

        hospitalSerializer = HospitalSerializer(hospitals, many=True)

        return Response(hospitalSerializer.data, status=status.HTTP_200_OK)

class DoctorApiView(APIView):

    def get(self, request):
        doctors = Doctor.objects.all()

        doctorSerializer = DoctorSerializer(doctors, many=True)

        return Response(doctorSerializer.data, status=status.HTTP_200_OK)

class TicketApiView(APIView):

    def get(self, request):

        hospitalId = request.GET.get('id_hospital', '')
        doctorId = request.GET.get('id_doctor', '')
        startDate = request.GET.get('start_date', '')
        endDate = request.GET.get('end_date', '')

        ticketsForm = TicketForm(request.GET)

        if ticketsForm.is_valid():

            if startDate != '' and endDate != '':

                tickets = Ticket.objects.filter(
                    hospital_id=hospitalId,
                    doctor_id=doctorId,
                    date__gte=startDate,
                    date__lte=endDate
                )

            elif startDate != '' and endDate == '':

                tickets = Ticket.objects.filter(
                    hospital_id=hospitalId,
                    doctor_id=doctorId,
                    date__gte=startDate,
                )

            elif startDate == '' and endDate != '':

                tickets = Ticket.objects.filter(
                    hospital_id=hospitalId,
                    doctor_id=doctorId,
                    date__lte=endDate
                )

            else:
                tickets = Ticket.objects.filter(
                    hospital_id=hospitalId,
                    doctor_id=doctorId
                )

            ticketSerializer = TicketsSerializer(tickets, many=True)

            return Response(ticketSerializer.data, status=status.HTTP_200_OK)
        return Response({'message': 'invalid data in query-string'}, status=status.HTTP_200_OK)
