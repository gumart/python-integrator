from rest_framework import serializers

from integrator.models import Hospital, Doctor, Ticket


class HospitalSerializer(serializers.ModelSerializer):
    id_hospital = serializers.SerializerMethodField()

    class Meta:
        model = Hospital
        fields = ('id_hospital', 'address')

    def get_id_hospital(self, hospital):
        return hospital.id

class DoctorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Doctor
        fields = ('id', 'fullname', 'hospital_id')


class TicketsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ticket
        fields = ('id', 'date', 'begin_time', 'end_time', 'is_closed', 'hospital_id', 'doctor_id')
