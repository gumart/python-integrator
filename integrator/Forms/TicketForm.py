from django import forms


class TicketForm(forms.Form):
    id_hospital = forms.IntegerField(required=True)
    id_doctor = forms.IntegerField(required=True)
    start_date = forms.DateField(required=False)
    end_date = forms.DateField(required=False)